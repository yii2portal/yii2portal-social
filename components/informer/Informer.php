<?php


namespace yii2portal\social\components\informer;

use Yii;
use yii2portal\core\components\Widget;

class Informer extends Widget
{

    public function insert($view, $title, $link, $id = null)
    {


        return $this->render($view, [
            'title' => $title,
            'link' => $link,
            'id' => $id,
        ]);
    }
}